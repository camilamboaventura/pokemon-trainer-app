import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from '../models/user.model';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  private _users: User[] = [];
  private _error: string = '';

  //DI - Dependency Injection: a way to share code between different files
  constructor(private readonly http: HttpClient) {}

  public fetchUsers(name: string): void {
    //angular doesn't use promisse for http request, it use Observables
    //Observables
    this.http.get<User[]>('http://localhost:3000/users?name=' + name).subscribe(
      (users: User[]) => {
        this._users = users;
      },
      (error: HttpErrorResponse) => {
        this._error = error.message;
      }
    );
  }
  public users(): User[] {
    return this._users;
  }

  public error(): string {
    return this._error;
  }
}
