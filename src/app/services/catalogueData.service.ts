import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class CatalogueData {
  constructor(private readonly http: HttpClient) {}

  //Get Pokemons
  getPokemons() {
    return this.http.get('https://pokeapi.co/api/v2/pokemon?limit=54');
  }

  //Get more Pokemons Data
  getMoreData(name: string) {
    return this.http.get(`https://pokeapi.co/api/v2/pokemon/${name}`);
  }
}
