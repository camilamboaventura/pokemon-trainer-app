import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { User } from '../models/user.model';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent {
  //lets the child send data to a parent component.
  @Output() success: EventEmitter<void> = new EventEmitter<void>();

  constructor(private readonly userService: UserService) {}

  public onSubmit(loginForm: NgForm): void {
    const { name } = loginForm.value; //accessing the input "name" of the loginForm
    const user: User = { name: name, pokemons: [] }; //creating a new User object
    const json: string = JSON.stringify(user); //transforming user in a json string
    const encoded: string = btoa(encodeURIComponent(json)); //encoding the string to save in the localStorage
    localStorage.setItem('loggedInUser', encoded); //saving in the localStorage
    this.success.emit(); //activating the success event configured in the template
  }
}
