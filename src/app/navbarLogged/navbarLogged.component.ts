import { Component } from '@angular/core';
import { User } from '../models/user.model';

@Component({
  selector: 'app-navbar-logged',
  templateUrl: './navbarLogged.component.html',
  styleUrls: ['./navbarLogged.component.css'],
})
export class NavbarLogged {
  public user(): User {
    const loggedInUser: string | null = localStorage.getItem('loggedInUser'); //pegando o usuario
    if (!loggedInUser) {
      throw new Error('Something bad happened');
    }
    const decoded: string = decodeURIComponent(atob(loggedInUser));
    return JSON.parse(decoded) as User;
  }

  public logout() {
    localStorage.removeItem('loggedInUser');
  }
}
