import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { LoginPage } from './loginPage/login.page';
import { Profile } from './profile/profile.component';
import { ProfilePage } from './profilePage/profile.page';
import { CataloguePage } from './pokemonCataloguePage/pokemonCatalogue.page';
import { PokemonCatalogue } from './pokemonCatalogue/pokemonCatalogue.component';
import { Navbar } from './Navbar/navbar.component';
import { NavbarLogged } from './navbarLogged/navbarLogged.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    LoginPage,
    Profile,
    ProfilePage,
    CataloguePage,
    PokemonCatalogue,
    Navbar,
    NavbarLogged,
  ],
  imports: [BrowserModule, HttpClientModule, FormsModule, AppRoutingModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
