import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-users',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.css'],
})
export class LoginPage {
  constructor(private readonly router: Router) {}

  //redirecting to Catalogue Page after handling the success event
  public async onSuccess(): Promise<void> {
    await this.router.navigate(['catalogue']);
  }
}
