import { Component, OnInit } from '@angular/core';
import { User } from '../models/user.model';
import { CatalogueData } from '../services/catalogueData.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
})
export class Profile implements OnInit {
  pokemons: any[] = [];

  constructor(private dataCatalogue: CatalogueData) {}

  //getting the user
  public user(): User {
    const loggedInUser: string | null = localStorage.getItem('loggedInUser');
    if (!loggedInUser) {
      throw new Error('Something bad happened');
    }
    const decoded: string = decodeURIComponent(atob(loggedInUser));
    return JSON.parse(decoded) as User;
  }

  //get the user and show all caught pokemon
  ngOnInit(): void {
    this.user().pokemons.forEach((name: string) => {
      this.dataCatalogue.getMoreData(name).subscribe((uniqueResponse: any) => {
        this.pokemons.push(uniqueResponse);
        console.log(this.pokemons);
      });
    });
  }
}
