import { Component, OnInit } from '@angular/core';
import { User } from '../models/user.model';
import { CatalogueData } from '../services/catalogueData.service';

@Component({
  selector: 'app-pokemon-catalogue',
  templateUrl: './pokemonCatalogue.component.html',
  styleUrls: ['./pokemonCatalogue.component.css'],
})
export class PokemonCatalogue implements OnInit {
  pokemons: any[] = [];

  constructor(private dataCatalogue: CatalogueData) {}

  //add Pokemons in pokemons array and template will show all pokemons added
  ngOnInit(): void {
    this.dataCatalogue.getPokemons().subscribe((response: any) => {
      response.results.forEach((result: { name: string }) => {
        this.dataCatalogue
          .getMoreData(result.name)
          .subscribe((uniqueResponse: any) => {
            this.pokemons.push({
              ...uniqueResponse,
              caught: this.isPokemonCaught(result.name),
            });
          });
      });
    });
  }

  //checking if Pokemon already exists in the collection
  public isPokemonCaught(name: string) {
    return this.user().pokemons.includes(name);
  }

  //add the Pokémon to the trainer’s collection
  public onCatchClick(pokeName: string): void {
    const user: User = this.user();
    user.pokemons.push(pokeName);
    const json: string = JSON.stringify(user);
    const encoded: string = btoa(encodeURIComponent(json));
    localStorage.setItem('loggedInUser', encoded);
    alert('You caught a ' + pokeName);
    location.reload();
  }

  public user(): User {
    const loggedInUser: string | null = localStorage.getItem('loggedInUser'); //getting the user from the localStorage
    if (!loggedInUser) {
      throw new Error('Something bad happened');
    }
    const decoded: string = decodeURIComponent(atob(loggedInUser));
    return JSON.parse(decoded) as User;
  }
}
