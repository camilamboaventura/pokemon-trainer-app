import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginPage } from './loginPage/login.page';
import { CataloguePage } from './pokemonCataloguePage/pokemonCatalogue.page';
import { ProfilePage } from './profilePage/profile.page';
import { AuthGuard } from './services/authGuard.service';

//array to define a Router configuration
const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/login',
  },
  {
    path: 'login',
    component: LoginPage,
  },
  {
    path: 'profile',
    canActivate: [AuthGuard],
    component: ProfilePage,
  },
  {
    path: 'catalogue',
    canActivate: [AuthGuard],
    component: CataloguePage,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
